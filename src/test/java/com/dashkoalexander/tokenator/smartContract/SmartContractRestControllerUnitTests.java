package com.dashkoalexander.tokenator.smartContract;

import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.rest.SmartContractRestController;
import com.dashkoalexander.tokenator.services.SmartContractService;
import com.dashkoalexander.tokenator.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SmartContractRestController.class)
public class SmartContractRestControllerUnitTests {

    private static final String TEST_ADDRESS = "0xf1000467258Ac2aEd9b3c0433c57f7c57E8EC0B3";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SmartContractService smartContractService;

    @MockBean
    private UserService userService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Before
    public void before() {
        SmartContract smartContract = new SmartContract(TEST_ADDRESS, Collections.emptySet(), null, null);
        Mockito.when(smartContractService.getByAddress(smartContract.getAddress())).thenReturn(smartContract);
    }

    @Test
    public void givenAddress_thenReturnUserCount() throws Exception {
        SmartContract found = smartContractService.getByAddress(TEST_ADDRESS);
        Integer count = found.getUsers().size();

        mockMvc.perform(get("/api/v1/smart-contracts/" + TEST_ADDRESS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['users']").value(count));
    }
}
