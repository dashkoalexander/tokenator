package com.dashkoalexander.tokenator.user;

import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByUserName_thenReturnUser() {
        User given = new User("username", "password", "password", "email@gmail.com", null, true, true, null, null,null,null);
        entityManager.persist(given);
        entityManager.flush();

        User found = userRepository.findByUsername(given.getUsername());

        assertThat(found.getUsername()).isEqualTo(given.getUsername());
    }
}
