package com.dashkoalexander.tokenator.user;

import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.repositories.UserRepository;
import com.dashkoalexander.tokenator.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceUnitTests {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void before() {
        User user = new User("username", "password", "password", "email@gmail.com", null, true, true, null, null,null,null);
        user.setId(Long.valueOf(1));
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
    }

    @Test
    public void whenValidId_thenUserShouldBeFound() {
        Long id = Long.valueOf(1);
        User found = userService.getById(id);

        assertThat(found.getId()).isEqualTo(id);
    }
}
