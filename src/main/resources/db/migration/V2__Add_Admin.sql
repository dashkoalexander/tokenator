insert into users (id, username, password, active, locked) values (1, 'admin', 'password', true, false);
insert into user_roles (user_id, roles) values (1, 'USER'), (1, 'ADMIN');

alter sequence hibernate_sequence restart with 2;