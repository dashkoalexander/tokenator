create sequence hibernate_sequence start 1 increment 1;

create table wallets (
                    id bigserial not null,
                    name varchar(80) not null,
                    filename varchar(255) not null,
                    user_id bigserial,
                    primary key (id));

create table smart_contracts (
                    id bigserial not null,
                    address varchar(42) not null,
                    primary key (id));

create table user_smart_contracts (
                    user_id bigserial not null,
                    smart_contract_id bigserial not null,
                    primary key (user_id, smart_contract_id));

create table user_roles (
                    user_id bigserial not null,
                    roles varchar(255));

create table users (
                    id bigserial not null,
                    password varchar(255) not null,
                    username varchar(20) not null,
                    email varchar(50),
                    activation_code varchar(255),
                    active boolean,
                    locked boolean,
                    userpic varchar(255),
                    primary key (id));

alter table if exists wallets add constraint wallet_user_fk foreign key (user_id) references users;
alter table if exists user_roles add constraint role_user_fk foreign key (user_id) references users;

alter table if exists user_smart_contracts add constraint smart_contract_user_fk foreign key (user_id) references users;
alter table if exists user_smart_contracts add constraint user_smart_contract_fk foreign key (smart_contract_id) references smart_contracts;