<#import "reusable/template.ftl" as template>

<@template.template>
<h5>${username}</h5>
<#if userpicFilename??>
    <div>
        <img src="/img/${userpicFilename}" max-height="100" max-width="100" />
    </div>
</#if>
${message?ifExists}
<form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <input type="password" class="form-control ${(passwordError??)?string('is-invalid', '')}" name="password" placeholder="Password" />
        <#if passwordError??>
            <div class="invalid-feedback">
                ${passwordError}
            </div>
        </#if>
    </div>
    <div class="form-group">
        <input type="email" class="form-control ${(emailError??)?string('is-invalid', '')}" value="<#if (userProfileUpdate.email)??>${userProfileUpdate.email}<#elseIf email??>${email}</#if>" name="email" placeholder="Email" />
        <#if emailError??>
            <div class="invalid-feedback">
                ${emailError}
            </div>
        </#if>
    </div>
    <div class="form-group">
        <div class="custom-file">
            <input type="file" name="file" id="customFile" />
            <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
    </div>
    <input type="hidden" name="_csrf" value="${_csrf.token}" />
    <button class="btn btn-primary" type="submit">Save</button>
</form>
</@template.template>