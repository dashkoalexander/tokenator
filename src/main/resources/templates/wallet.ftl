<#import "reusable/template.ftl" as template>
<#import "reusable/sendERC20Token.ftl" as sendERC20Token>

<@template.template>

    <#include "reusable/sendEth.ftl" />
    <br/>
    <h5 class="mb-1">Current wallet:</h5>
    <div>
        <p>
            <span class="text-muted">${userWalletAddress}</span>
            <br>
            <#if walletBalance??>
                Balance: ${walletBalance} ETH
            <#else>
                Balance unavailable
            </#if>
        </p>
    </div>

    <br/>
    <@sendERC20Token.sendERC20Token>
        <#include "reusable/smartContractFullList.ftl" />
    </@sendERC20Token.sendERC20Token>
</@template.template>