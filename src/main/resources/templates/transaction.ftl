<#import "reusable/template.ftl" as template>

<@template.template>
<#if errorDescription??>
    <h3>Transaction failed</h3>
    <div>
        <p class="text-muted">${errorDescription}</p>
    </div>
<#else>
    <h3>${transactionDescription}</h3>
    <div>
        <p class="text-muted">Transaction hash: ${transactionHash}</p>
        <a href="${transactionExplorer}" target="_blank">View transaction state</a>
    </div>
</#if>
</@template.template>