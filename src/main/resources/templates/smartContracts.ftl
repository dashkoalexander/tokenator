<#import "reusable/template.ftl" as template>

<@template.template>
    <#include "reusable/addSmartContract.ftl" />
    <#include "reusable/smartContractBasicList.ftl" />
</@template.template>