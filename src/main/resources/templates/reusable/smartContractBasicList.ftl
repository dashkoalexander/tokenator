<#include "security.ftl">
<#import "pager.ftl" as pager>

<#if page.getTotalPages() gt 1>
    <@pager.pager url page />
</#if>

<ul class="list-group">
    <#list page.content as smartContract>
        <li class="list-group-item">
            <div class="d-flex w-100">
                <h5 class="mb-1"><#if smartContract.name??>${smartContract.name}</#if></h5>
            </div>
            <div class="d-flex w-100 justify-content-between">
                <p class="mb-1">${smartContract.address}</p>
                <a href="${addressExplorer}${smartContract.address}" target="_blank">
                    <button type="button" class="btn btn-info">View</button>
                </a>
            </div>
        </li>
    <#else>
        <li class="list-group-item">
            No token addresses were added yet.
        </li>
    </#list>
</ul>

<#if page.getTotalPages() gt 1>
    <@pager.pager url page />
</#if>