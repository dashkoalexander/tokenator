<#include "security.ftl">
<#import "pager.ftl" as pager>

<#if page.getTotalPages() gt 1>
    <@pager.pager url page />
</#if>

<ul class="list-group">
    <#list page.content as wallet>
        <li class="list-group-item">
                <div class="d-flex w-100">
                    <a href="/wallets/${wallet.id}">
                        <h5 class="mb-1">${wallet.name}</h5>
                    </a>
                </div>
                <#if wallet.address??>
                    <div class="d-flex w-100 justify-content-between">
                        <p class="mb-1">${wallet.address}</p>
                        <a href="${addressExplorer}${wallet.address}" target="_blank">
                            <button type="button" class="btn btn-info">View</button>
                        </a>
                    </div>
                </#if>
        </li>
    <#else>
        <li class="list-group-item">
            No wallets were created yet.
        </li>
    </#list>
</ul>

<#if page.getTotalPages() gt 1>
    <@pager.pager url page />
</#if>