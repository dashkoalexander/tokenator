<#macro sendERC20Token>
    <form method="post" action="/wallets/${wallet.id}/send-tokens">
        <h5 class="mb-1">Send tokens:</h5>
        <br/>
        <div class="form-group">
            <input type="text" class="form-control ${(tokenRecipientAddressError??)?string('is-invalid', '')}" value="<#if (erc20TokenTransaction.tokenRecipientAddress)??>${erc20TokenTransaction.tokenRecipientAddress}</#if>" name="tokenRecipientAddress" placeholder="Recipient Ethereum address" />
            <#if tokenRecipientAddressError??>
                <div class="invalid-feedback">
                    ${tokenRecipientAddressError}
                </div>
            </#if>
        </div>
        <div class="form-group">
            <input type="text" class="form-control ${(tokenAmountError??)?string('is-invalid', '')}" value="<#if (erc20TokenTransaction.tokenAmount)??>${erc20TokenTransaction.tokenAmount}</#if>" name="tokenAmount" placeholder="Amount of tokens to send" />
            <#if tokenAmountError??>
                <div class="invalid-feedback">
                    ${tokenAmountError}
                </div>
            </#if>
        </div>

        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <input type="hidden" name="userWalletAddress" value="<#if userWalletAddress??>${userWalletAddress}</#if>" />
        <#nested>
    </form>
</#macro>