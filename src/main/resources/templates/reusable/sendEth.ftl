<form method="post" action="/wallets/${wallet.id}/send-eth">
    <h5 class="mb-1">Send ETH:</h5>
    <br/>
    <div class="form-group">
        <input type="text" class="form-control ${(ethRecipientAddressError??)?string('is-invalid', '')}" value="<#if (ethTransaction.ethRecipientAddress)??>${ethTransaction.ethRecipientAddress}</#if>" name="ethRecipientAddress" placeholder="Recipient Ethereum address" />
        <#if ethRecipientAddressError??>
        <div class="invalid-feedback">
            ${ethRecipientAddressError}
        </div>
    </#if>
    </div>
    <div class="form-group">
        <input type="text" class="form-control ${(ethAmountError??)?string('is-invalid', '')}" value="<#if (ethTransaction.ethAmount)??>${ethTransaction.ethAmount}</#if>" name="ethAmount" placeholder="Amount of ETH to send" />
        <#if ethAmountError??>
        <div class="invalid-feedback">
            ${ethAmountError}
        </div>
    </#if>
    </div>

    <input type="hidden" name="_csrf" value="${_csrf.token}" />
    <input type="hidden" name="userWalletAddress" value="<#if userWalletAddress??>${userWalletAddress}</#if>" />
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Send ETH</button>
    </div>
</form>