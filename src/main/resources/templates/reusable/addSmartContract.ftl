<div class="form-group mt-3">
    <form method="post" action="/add-token">
        <div class="form-group">
            <input type="text" class="form-control ${(addressError??)?string('is-invalid', '')}" value="<#if (smartContract.address)??>${smartContract.address}</#if>" name="address" placeholder="Token Ethereum address" />
            <#if addressError??>
                <div class="invalid-feedback">
                    ${addressError}
                </div>
            </#if>
        </div>

        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <input type="hidden" name="id" value="<#if (smartContract.id)??>${smartContract.id}</#if>" />
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add Token</button>
        </div>
    </form>
</div>