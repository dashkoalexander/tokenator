<#include "security.ftl">
<#import "pager.ftl" as pager>

<#if page.getTotalPages() gt 1>
    <@pager.pager url page />
</#if>

<ul class="list-group">
    <#list page.content as smartContract>
        <li class="list-group-item">
            <div class="d-flex w-100">
                <h5 class="mb-1"><#if smartContract.name??>${smartContract.name}</#if></h5>
            </div>
            <div class="d-flex w-100 justify-content-between">
                <p>
                    <span class="text-muted">${smartContract.address}</span>
                    <br>
                    <#if walletBalance??>
                        Balance: ${smartContract.balance} tokens
                    <#else>
                        Balance unavailable
                    </#if>
                </p>
                <button type="submit" class="btn btn-primary" name="smartContractAddress" value="${smartContract.address}">Send tokens</button>
            </div>
        </li>
    <#else>
        <li class="list-group-item">
            No token addresses were added yet.
        </li>
    </#list>
</ul>

<#if page.getTotalPages() gt 1>
    <@pager.pager url page />
</#if>