<div class="form-group mt-3">
    <form method="post" action="/create-wallet">
        <div class="form-group">
            <input type="text" class="form-control ${(nameError??)?string('is-invalid', '')}" value="<#if (wallet.name)??>${wallet.name}</#if>" name="name" placeholder="Wallet name" />
            <#if nameError??>
                <div class="invalid-feedback">
                    ${nameError}
                </div>
            </#if>
        </div>

        <input type="hidden" name="_csrf" value="${_csrf.token}" />
        <input type="hidden" name="id" value="<#if (wallet.id)??>${wallet.id}</#if>" />
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Create Wallet</button>
        </div>
    </form>
</div>