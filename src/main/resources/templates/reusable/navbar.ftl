<#include "security.ftl">
<#import "login.ftl" as login>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Tokenator</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <#if known>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <#if user??>
                    <li class="nav-item">
                        <a class="nav-link" href="/wallets">My wallets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tokens">My tokens</a>
                    </li>
                </#if>
                <#if isAdmin>
                    <li class="nav-item">
                        <a class="nav-link" href="/user">User list</a>
                    </li>
                </#if>
                <#if user??>
                    <li class="nav-item">
                        <a class="nav-link" href="/user/profile">Profile</a>
                    </li>
                </#if>
            </ul>
            <#if user??>
                <div class="navbar-text mr-3">${name}</div>
                <@login.logout/>
            <#else>
                <a href="/login">
                    <button class="btn btn-primary">Log in</button>
                </a>
            </#if>
        </div>
    </#if>
</nav>