<#import "reusable/template.ftl" as template>
<#import "reusable/login.ftl" as login>

<@template.template>
<h5 class="mb-1">Add new user</h5>
<br>
${message?ifExists}
<@login.login "/registration" true />
</@template.template>