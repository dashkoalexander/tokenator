<#import "../reusable/template.ftl" as template>

<@template.template>
    <h5 class="mb-1">The requested page is not found!</h5>
</@template.template>