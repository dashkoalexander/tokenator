<#import "reusable/template.ftl" as template>

<@template.template>
    <#include "reusable/createWallet.ftl" />
    <#include "reusable/walletList.ftl" />
</@template.template>