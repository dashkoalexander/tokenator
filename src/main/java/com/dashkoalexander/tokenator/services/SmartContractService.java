package com.dashkoalexander.tokenator.services;

import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.models.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigInteger;

/**interface {@code SmartContractService} for SmartContractServiceImpl which will handle
 * all the interactions with smart contracts database
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.SmartContractServiceImpl
 */
public interface SmartContractService {
    SmartContract getByAddress(String address);
    boolean addSmartContract(SmartContract smartContract, User user);
    Page<SmartContract> getUserSmartContracts(User user, Pageable pageable);
    void getSmartContractBalances(String walletAddress, Page<SmartContract> smartContracts, User user);
    void getSmartContractNames(String walletAddress, Page<SmartContract> smartContracts, User user);
    String sendERC20Tokens(Wallet wallet, User user, String smartContractAddress, String recipientAddress, BigInteger tokensAmount);
}
