package com.dashkoalexander.tokenator.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**class {@code MailSenderService} uses the JavaMailSender bean for
 * confirming users' email addresses by sending a templated mail with
 * a pre-generated activation link
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.config.MailConfig
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MailSenderService {

    private final JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String username;

    public void send(String emailTo, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(username);
        mailMessage.setTo(emailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);

        new Thread(() -> { mailSender.send(mailMessage); }).start();
    }
}
