package com.dashkoalexander.tokenator.services;

import com.dashkoalexander.tokenator.models.Role;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**class {@code UserServiceImpl} uses the UserRepository and PasswordEncoder beans to handle
 * all the interactions with users database, also sends activation emails with
 * the MailSenderService bean
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.controllers.UserController
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final MailSenderService mailSenderService;
    private final PasswordEncoder passwordEncoder;

    @Value("${hostname}")
    private String hostname;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public User getById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void updateUserEmailAndPassword(User user, String email, String password) {
        boolean isUserUpdated = false;
        boolean isUserEmailChanged = email != null && !email.equals(user.getEmail());

        if(isUserEmailChanged) {
            isUserUpdated = true;
            user.setEmail(email);
            if (!StringUtils.isBlank(email)) {
                user.setActivationCode(UUID.randomUUID().toString());
                sendActivationMessage(user);
            }
        }

        if(!StringUtils.isBlank(password)) {
            isUserUpdated = true;
            user.setPassword(passwordEncoder.encode(password));
        }

        if(isUserUpdated){
            userRepository.save(user);
        }
    }

    @Override
    public boolean createUser(User user) {
        User userFromDb = userRepository.findByUsername(user.getUsername());

        if (userFromDb != null) {
            return false;
        }

        user.setActive(true);
        user.setLocked(false);
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        save(user);
        sendActivationMessage(user);

        return true;
    }

    private void sendActivationMessage(User user) {
        if (!StringUtils.isBlank(user.getEmail())) {
            String message = String.format(
                    "Hello, %s!\nWelcome to Tokenator. Please, visit the link to activate your email: http://%s/activate/%s",
                    user.getUsername(),
                    hostname,
                    user.getActivationCode()
            );

            mailSenderService.send(user.getEmail(), "Activation code", message);
        }
    }

    public boolean activateUser(String code) {
        User user = userRepository.findByActivationCode(code);
        if (user == null) {
            return false;
        }

        user.setActivationCode(null);
        userRepository.save(user);
        return true;
    }

    @Override
    public void editUser(User user, String username, Map<String, String> form) {
        user.setUsername(username);

        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        for(String key : form.keySet())
        {
            if(roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        save(user);
    }
}
