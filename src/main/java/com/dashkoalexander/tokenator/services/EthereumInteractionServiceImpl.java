package com.dashkoalexander.tokenator.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.tx.Contract;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.web3j.tx.ManagedTransaction.GAS_PRICE;

/**class {@code EthereumInteractionServiceImpl} uses the Web3J bean for all user interactions with
 * Ethereum network, wallets creation, transfers of both ETH and ERC-20 tokens
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.controllers.SmartContractController
 * @see com.dashkoalexander.tokenator.controllers.WalletController
 * @see com.dashkoalexander.tokenator.services.WalletServiceImpl
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EthereumInteractionServiceImpl implements EthereumInteractionService {

    private final Web3j web3j;

    @Value("${wallets.path}")
    private String walletsPath;

    @Override
    public String createWallet(String password) throws CipherException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        File walletsDir = new File(walletsPath);

        if (!walletsDir.exists()) {
            walletsDir.mkdir();
        }

        String fileName = WalletUtils.generateFullNewWalletFile(password, walletsDir);
        return new File(fileName).getName();
    }

    @Override
    public String getWalletAddress(String filename, String password) throws IOException, CipherException {
        File file = new File(walletsPath + filename);
        Credentials credentials = WalletUtils.loadCredentials(password, file);
        return credentials.getAddress();
    }

    @Override
    public BigDecimal getEthBalance(String address) throws ExecutionException, InterruptedException {
        EthGetBalance result = web3j.ethGetBalance(address, DefaultBlockParameter.valueOf("latest"))
                .sendAsync().get();
        return Convert.fromWei(result.getBalance().toString(), Convert.Unit.ETHER);
    }

    @Override
    public BigInteger getERC20Balance(String senderAddress, String contractAddress) throws ExecutionException, InterruptedException {
        List<Type> returnValues = callSmartContractViewFunction(
                                                                senderAddress,
                                                                contractAddress,
                                                                "balanceOf",
                                                                Arrays.asList(new Address(senderAddress)),
                                                                Arrays.asList(new TypeReference<Uint256>() {}));

        BigInteger balance = this.<BigInteger>getFirstReturnValue(returnValues, Uint256.class, BigInteger.valueOf(0));
        BigInteger decimals = BigInteger.valueOf(10).pow(getERC20Decimals(senderAddress, contractAddress).intValue());
        return balance.divide(decimals);
    }

    @Override
    public BigInteger getERC20Decimals(String senderAddress, String contractAddress) throws ExecutionException, InterruptedException {
        List<Type> returnValues = callSmartContractViewFunction(
                                                                senderAddress,
                                                                contractAddress,
                                                                "decimals",
                                                                Arrays.asList(),
                                                                Arrays.asList(new TypeReference<Uint256>() {}));

        return this.<BigInteger>getFirstReturnValue(returnValues, Uint256.class, BigInteger.valueOf(0));
    }

    @Override
    public String getERC20Name(String senderAddress, String contractAddress) throws ExecutionException, InterruptedException {
        List<Type> returnValues = callSmartContractViewFunction(
                senderAddress,
                contractAddress,
                "name",
                Arrays.asList(),
                Arrays.asList(new TypeReference<Utf8String>() {}));

        return this.<String>getFirstReturnValue(returnValues, Utf8String.class, "-");
    }

    /**
     * calling a Solidity smart contract function that won't cost any gas
     */
    private List<Type> callSmartContractViewFunction(String senderAddress, String contractAddress, String functionName, List<Type> inputValues, List<TypeReference<?>> outputValues) throws ExecutionException, InterruptedException {
        Function function = new Function(functionName, inputValues, outputValues);

        String encodedFunction = FunctionEncoder.encode(function);
        EthCall response = web3j.ethCall(Transaction.createEthCallTransaction(senderAddress, contractAddress, encodedFunction), DefaultBlockParameterName.LATEST)
                .sendAsync().get();

        List<Type> returnValues = FunctionReturnDecoder.decode(response.getValue(), function.getOutputParameters());
        return returnValues;
    }

    private <T> T getFirstReturnValue(List<Type> returnValues, Class typeClass, T defaultValue) {
        Type returnValue = returnValues.stream()
                                        .filter(el -> el.getClass().equals(typeClass))
                                        .findFirst()
                                        .orElse(null);

        if(returnValue == null){
            return defaultValue;
        } else {
            return (T) returnValue.getValue();
        }
    }

    @Override
    public String sendEth(String filename, String password, String toAddress, BigDecimal amount) throws Exception {
        File file = new File(walletsPath + filename);
        Credentials credentials = WalletUtils.loadCredentials(password, file);

        RawTransaction rawTransaction = RawTransaction.createEtherTransaction(getTransactionCount(credentials.getAddress()), GAS_PRICE, Contract.GAS_LIMIT, toAddress, Convert.toWei(amount, Convert.Unit.ETHER).toBigInteger());

        byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signedMessage);

        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).sendAsync().get();

        if(ethSendTransaction.hasError()) {
            return ethSendTransaction.getError().getMessage();
        } else {
            return ethSendTransaction.getTransactionHash();
        }
    }

    @Override
    public String transferERC20Tokens(String filename, String password, String contractAddress, String toAddress, BigInteger tokens) throws ExecutionException, InterruptedException, IOException, CipherException {
        File file = new File(walletsPath + filename);
        Credentials credentials = WalletUtils.loadCredentials(password, file);

        Address recipientAddress = new Address(toAddress);
        Uint256 transferAmount = new Uint256(tokens.multiply(getERC20Decimals(credentials.getAddress(), contractAddress)));
        Function function = new Function(
                "transfer",
                Arrays.asList(recipientAddress, transferAmount),
                Arrays.asList(new TypeReference<Type>() {}));

        String encodedFunction = FunctionEncoder.encode(function);
        RawTransaction rawTransaction = RawTransaction.createTransaction(getTransactionCount(credentials.getAddress()), GAS_PRICE, Contract.GAS_LIMIT, contractAddress, encodedFunction);

        byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signedMessage);

        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).sendAsync().get();

        if(ethSendTransaction.hasError()) {
            return ethSendTransaction.getError().getMessage();
        } else {
            return ethSendTransaction.getTransactionHash();
        }

        /*
        Function function = new Function(
                "transfer",
                Arrays.asList(new Address(toAddress), new Uint256(tokens.multiply(getERC20Decimals(senderAddress, contractAddress)))),
                Arrays.asList(new TypeReference<Type>() {}));

        String encodedFunction = FunctionEncoder.encode(function);
        Transaction transaction = Transaction.createFunctionCallTransaction(senderAddress, getTransactionCount(senderAddress), GAS_PRICE, Contract.GAS_LIMIT, contractAddress, encodedFunction);

        EthSendTransaction transactionResponse = web3j.ethSendTransaction(transaction)
                .sendAsync().get();

        return transactionResponse.getTransactionHash();
        */
    }

    private BigInteger getTransactionCount(String address) throws ExecutionException, InterruptedException {
        EthGetTransactionCount result = web3j.ethGetTransactionCount(address, DefaultBlockParameter.valueOf("latest"))
                .sendAsync().get();
        return result.getTransactionCount();
    }
}