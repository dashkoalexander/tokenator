package com.dashkoalexander.tokenator.services;

import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.models.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;

/**interface {@code WalletService} for WalletServiceImpl which will handle
 * all the interactions with wallets database
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.WalletServiceImpl
 */
public interface WalletService {
    boolean createWallet(String name, User user);
    Page<Wallet> getUserWallets(User targetUser, Pageable pageable);
    void getWalletAddresses(Page<Wallet> wallets, User user);
    String getWalletAddress(Wallet wallet, User user);
    BigDecimal getWalletBalance(String walletAddress);
    String sendEth(Wallet wallet, User user, String recepientAddress, BigDecimal ethAmount);
}
