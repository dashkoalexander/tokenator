package com.dashkoalexander.tokenator.services;

import com.dashkoalexander.tokenator.exceptions.ExpectedExceptionMethodExecutor;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.models.Wallet;
import com.dashkoalexander.tokenator.repositories.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**class {@code WalletServiceImpl} uses the WalletRepository bean to handle
 * all the interactions with wallets database
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.controllers.WalletController
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final EthereumInteractionService ethereumInteractionService;

    @Override
    @Transactional
    public boolean createWallet(String name, User user) {
        String password = user.getPassword();
        Wallet wallet = new Wallet(ExpectedExceptionMethodExecutor.<String>execute(() -> ethereumInteractionService.createWallet(password)),
                                    name,
                                    user,
                                    null);
        walletRepository.save(wallet);
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Wallet> getUserWallets(User targetUser, Pageable pageable) {
        return walletRepository.findByUser(targetUser, pageable);
    }

    @Override
    public void getWalletAddresses(Page<Wallet> wallets, User user) {
        wallets.stream()
                .forEach(el ->
                        el.setAddress(ExpectedExceptionMethodExecutor.<String>execute(() -> ethereumInteractionService.getWalletAddress(el.getFilename(), user.getPassword()))));
    }

    @Override
    public String getWalletAddress(Wallet wallet, User user) {
        return ExpectedExceptionMethodExecutor.<String>execute(() -> ethereumInteractionService.getWalletAddress(wallet.getFilename(), user.getPassword()));
    }

    @Override
    public BigDecimal getWalletBalance(String walletAddress) {
        return ExpectedExceptionMethodExecutor.<BigDecimal>execute(() -> ethereumInteractionService.getEthBalance(walletAddress));
    }

    @Override
    public String sendEth(Wallet wallet, User user, String recepientAddress, BigDecimal ethAmount) {
        return ExpectedExceptionMethodExecutor.<String>execute(() -> ethereumInteractionService.sendEth(wallet.getFilename(), user.getPassword(), recepientAddress, ethAmount));
    }
}
