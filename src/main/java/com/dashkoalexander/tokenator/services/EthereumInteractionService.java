package com.dashkoalexander.tokenator.services;

import org.web3j.crypto.CipherException;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.concurrent.ExecutionException;

/**interface {@code EthereumInteractionService} for EthereumInteractionServiceImpl which
 * will handle all user interactions with Ethereum network, wallets creation, transfers of
 * both ETH and ERC-20 tokens
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.EthereumInteractionServiceImpl
 */
public interface EthereumInteractionService {
    String createWallet(String password) throws CipherException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, IOException;
    String getWalletAddress(String filename, String password) throws IOException, CipherException;
    BigDecimal getEthBalance(String address) throws ExecutionException, InterruptedException;
    BigInteger getERC20Balance(String senderAddress, String contractAddress) throws ExecutionException, InterruptedException;
    BigInteger getERC20Decimals(String senderAddress, String contractAddress) throws ExecutionException, InterruptedException;
    String getERC20Name(String senderAddress, String contractAddress) throws ExecutionException, InterruptedException;
    String sendEth(String filename, String password, String toAddress, BigDecimal amount) throws Exception;
    String transferERC20Tokens(String filename, String password, String contractAddress, String toAddress, BigInteger tokens) throws ExecutionException, InterruptedException, IOException, CipherException;
}
