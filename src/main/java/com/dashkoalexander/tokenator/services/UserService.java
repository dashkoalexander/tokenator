package com.dashkoalexander.tokenator.services;

import com.dashkoalexander.tokenator.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Map;

/**interface {@code UserService} for UserServiceImpl which will handle
 * all the interactions with users database, send activation emails
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.UserServiceImpl
 */
public interface UserService extends UserDetailsService {
    List<User> getAll();
    User getById(Long id);
    void save(User user);
    void deleteById(Long id);
    void editUser(User user, String username, Map<String, String> form);
    void updateUserEmailAndPassword(User user, String email, String password);
    boolean createUser(User user);
    boolean activateUser(String code);
}
