package com.dashkoalexander.tokenator.services;

import com.dashkoalexander.tokenator.exceptions.ExpectedExceptionMethodExecutor;
import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.models.Wallet;
import com.dashkoalexander.tokenator.repositories.SmartContractRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

/**class {@code SmartContractServiceImpl} uses the SmartContractRepository bean to handle
 * all the interactions with smart contracts database
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.controllers.SmartContractController
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SmartContractServiceImpl implements SmartContractService {

    private final SmartContractRepository smartContractRepository;
    private final EthereumInteractionService ethereumInteractionService;

    @Override
    @Transactional(readOnly = true)
    public SmartContract getByAddress(String address) {
        return smartContractRepository.findByAddress(address);
    }

    @Override
    @Transactional
    public boolean addSmartContract(SmartContract smartContract, User user) {
        SmartContract smartContractFromDb = smartContractRepository.findByAddress(smartContract.getAddress());

        if (smartContractFromDb != null) {
            smartContract = smartContractFromDb;
        }

        smartContract.getUsers().add(user);
        smartContractRepository.save(smartContract);
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SmartContract> getUserSmartContracts(User user, Pageable pageable) {
        return smartContractRepository.findByUsers(user, pageable);
    }

    @Override
    public void getSmartContractBalances(String walletAddress, Page<SmartContract> smartContracts, User user) {
        smartContracts.stream()
                        .forEach(el ->
                                el.setBalance(ExpectedExceptionMethodExecutor.<BigInteger>execute(() -> ethereumInteractionService.getERC20Balance(walletAddress, el.getAddress()))));
    }

    @Override
    public void getSmartContractNames(String walletAddress, Page<SmartContract> smartContracts, User user) {
        smartContracts.stream()
                        .forEach(el ->
                                el.setName(ExpectedExceptionMethodExecutor.<String>execute(() -> ethereumInteractionService.getERC20Name(walletAddress, el.getAddress()))));
    }

    @Override
    public String sendERC20Tokens(Wallet wallet, User user, String smartContractAddress, String recipientAddress, BigInteger tokensAmount) {
        return ExpectedExceptionMethodExecutor.<String>execute(() -> ethereumInteractionService.transferERC20Tokens(wallet.getFilename(), user.getPassword(), smartContractAddress, recipientAddress, tokensAmount));
    }
}
