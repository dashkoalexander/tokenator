package com.dashkoalexander.tokenator.exceptions;

public interface ExpectedExceptionMethodCallWithReturn<T> {
    T action() throws Exception;
}
