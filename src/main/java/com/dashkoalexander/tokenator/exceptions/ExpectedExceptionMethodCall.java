package com.dashkoalexander.tokenator.exceptions;

public interface ExpectedExceptionMethodCall {
    void action() throws Exception;
}
