package com.dashkoalexander.tokenator.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**class {@code ApplicationExceptionHandler} logs and processes both
 * expected and unexpected exceptions
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.exceptions.ExpectedException
 */
@ControllerAdvice(basePackages = {"com.dashkoalexander.tokenator.services", "com.dashkoalexander.tokenator.controllers"})
public class ApplicationExceptionHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

    @ExceptionHandler(ExpectedException.class)
    public String handleExpectedException(ExpectedException ex) {
        return handleExceptionInternal(ex, ex.getExceptionType().getHttpStatus());
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception ex) {
        LOGGER.error("Error:", ex);
        return handleExceptionInternal(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private String handleExceptionInternal(Exception ex, HttpStatus httpStatus) {
        LOGGER.error("Error:", ex);

        switch (httpStatus)
        {
            case INTERNAL_SERVER_ERROR:
                return "error/5xx";
            case NOT_FOUND:
                return "error/404";
            default:
                return "error/error";
        }
    }
}
