package com.dashkoalexander.tokenator.exceptions;

/**class {@code ExpectedExceptionMethodExecutor} utilizes the ExpectedExceptionMethodCallWithReturn
 * for wrapping the code with a potential exception generation into
 * safe try-catch clause with further exception logging and processing in
 * the ApplicationExceptionHandler
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.exceptions.ExpectedExceptionMethodCallWithReturn
 * @see com.dashkoalexander.tokenator.exceptions.ApplicationExceptionHandler
 */
public class ExpectedExceptionMethodExecutor {
    public static <T> T execute(ExpectedExceptionMethodCallWithReturn<T> expectedExceptionMethodCallWithReturn)
    {
        try {
            return expectedExceptionMethodCallWithReturn.action();
        } catch (Exception ex) {
            throw new ExpectedException(ExceptionType.INTERNAL_SERVER_ERROR, ex);
        }
    }
}
