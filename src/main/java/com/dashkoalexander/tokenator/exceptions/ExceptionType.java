package com.dashkoalexander.tokenator.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ExceptionType {

    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "An internal server error occurred."),
    NOT_FOUND(HttpStatus.NOT_FOUND, "The requested page was not found.");

    private HttpStatus httpStatus;
    private String message;
}
