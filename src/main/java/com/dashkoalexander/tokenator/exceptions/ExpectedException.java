package com.dashkoalexander.tokenator.exceptions;

import lombok.Getter;

import java.text.MessageFormat;

/**class {@code ExpectedException} is used for exception logging and processing in
 * the ApplicationExceptionHandler
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.exceptions.ApplicationExceptionHandler
 */
@Getter
public class ExpectedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final ExceptionType exceptionType;

    public ExpectedException(ExceptionType exceptionType, Object... messageArguments) {
        super(MessageFormat.format(exceptionType.getMessage(), messageArguments));
        this.exceptionType = exceptionType;
    }

    public ExpectedException(ExceptionType exceptionType, final Throwable cause, Object... messageArguments) {
        super(MessageFormat.format(exceptionType.getMessage(), messageArguments), cause);
        this.exceptionType = exceptionType;
    }
}
