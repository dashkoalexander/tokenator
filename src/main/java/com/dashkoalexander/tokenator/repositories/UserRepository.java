package com.dashkoalexander.tokenator.repositories;

import com.dashkoalexander.tokenator.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**interface {@code UserRepository} provides the Spring Data JpaRepository bean
 * for a User entity to be used in the UserServiceImpl
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.UserServiceImpl
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
    User findByActivationCode(String code);
}
