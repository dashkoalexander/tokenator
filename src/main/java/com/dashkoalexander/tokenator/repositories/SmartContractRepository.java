package com.dashkoalexander.tokenator.repositories;

import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**interface {@code SmartContractRepository} provides the Spring Data JpaRepository bean
 * for a SmartContract entity to be used in the SmartContractServiceImpl
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.SmartContractServiceImpl
 */
public interface SmartContractRepository extends JpaRepository<SmartContract, Long> {

    SmartContract findByAddress(String address);

    //either
    //@Query("select smartContract from SmartContract as smartContract join smartContract.users as user where user = :targetUser")
    //Page<SmartContract> findByUser(@Param("targetUser") User targetUser, Pageable pageable);
    //or just
    Page<SmartContract> findByUsers(User user, Pageable pageable);
}
