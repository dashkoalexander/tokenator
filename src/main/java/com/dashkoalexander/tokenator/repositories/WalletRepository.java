package com.dashkoalexander.tokenator.repositories;

import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.models.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**interface {@code WalletRepository} provides the Spring Data JpaRepository bean
 * for a Wallet entity to be used in the WalletServiceImpl
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.WalletServiceImpl
 */
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    @Query("from Wallet as wallet where wallet.user = :targetUser")
    Page<Wallet> findByUser(@Param("targetUser") User targetUser, Pageable pageable);
}
