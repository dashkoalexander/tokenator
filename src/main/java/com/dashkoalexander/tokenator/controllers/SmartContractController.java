package com.dashkoalexander.tokenator.controllers;

import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.services.SmartContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

/**class {@code SmartContractController} utilizes the SmartContractServiceImpl
 * and EthereumInteractionServiceImpl beans for enabling user's
 * smart contract-related operations
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.SmartContractServiceImpl
 */
@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SmartContractController {

    private final SmartContractService smartContractService;

    @Value("${address.explorer}")
    private String addressExplorer;

    @Value("${default.view.address}")
    private String defaultViewAddress;

    @GetMapping("/tokens")
    public String getSmartContracts(
            @AuthenticationPrincipal User user,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ){
        putUserSmartContractsToModel(model, user, pageable);
        return "/smartContracts";
    }

    @PostMapping("/add-token")
    public String addSmartContract(
            @AuthenticationPrincipal User user,
            @Valid SmartContract smartContract,
            BindingResult bindingResult,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        if(bindingResult.hasErrors()) {
            ControllerUtils.putErrorsToModel(bindingResult, model);
            model.addAttribute("smartContract", smartContract);
        } else {
            model.addAttribute("smartContract", null);
            smartContractService.addSmartContract(smartContract, user);
        }

        putUserSmartContractsToModel(model, user, pageable);
        return "/smartContracts";
    }

    private void putUserSmartContractsToModel(Model model, User user, Pageable pageable) {
        Page<SmartContract> smartContracts = smartContractService.getUserSmartContracts(user, pageable);
        smartContractService.getSmartContractNames(defaultViewAddress, smartContracts, user);

        model.addAttribute("page", smartContracts);
        model.addAttribute("url", "/tokens");
        model.addAttribute("addressExplorer", addressExplorer);
    }
}
