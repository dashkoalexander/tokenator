package com.dashkoalexander.tokenator.controllers;

import com.dashkoalexander.tokenator.models.DTO.UserProfileUpdateDTO;
import com.dashkoalexander.tokenator.models.Role;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.services.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Map;

/**class {@code UserController} utilizes the UserServiceImpl bean
 * for enabling user's profile-related operations
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.UserServiceImpl
 */
@Controller
@RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @Value("${uploads.path}")
    private String uploadsPath;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public String getUsers(Model model) {
        Iterable<User> users = userService.getAll();
        model.addAttribute("users", users);
        return "/users";
    }

    @GetMapping("{user}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String editUser(
            @PathVariable User user,
            Model model
    ) {
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "/userEdit";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public String editUser(
            @RequestParam String username,
            @RequestParam Map<String, String> form,
            @RequestParam("userId") User user
    ) {
        userService.editUser(user, username, form);
        return "redirect:/user";
    }

    @GetMapping("/profile")
    public String getProfile(
            @AuthenticationPrincipal User user,
            Model model
    ) {
        if(!StringUtils.isBlank(user.getUserpicFilename()))
        {
            model.addAttribute("userpicFilename", user.getUserpicFilename());
        }
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        return "/profile";
    }

    @PostMapping("/profile")
    public String updateProfile(
            @AuthenticationPrincipal User user,
            @Valid UserProfileUpdateDTO userProfileUpdateDTO,
            @RequestParam("file") MultipartFile file,
            BindingResult bindingResult,
            Model model
    ) {
        if(bindingResult.hasErrors()) {
            ControllerUtils.putErrorsToModel(bindingResult, model);
            model.addAttribute("userProfileUpdate", userProfileUpdateDTO);
        } else {
            userService.updateUserEmailAndPassword(user, userProfileUpdateDTO.getEmail(), userProfileUpdateDTO.getPassword());
            model.addAttribute("message", "Profile was updated successfully.");
            model.addAttribute("userProfileUpdate", null);

            String newUserpicFilename = ControllerUtils.saveUserpicFile(file, uploadsPath);
            if(!StringUtils.isBlank(newUserpicFilename))
            {
                model.addAttribute("userpicFilename", newUserpicFilename);
                String oldUserpicFilename = user.getUserpicFilename();
                user.setUserpicFilename(newUserpicFilename);
                userService.save(user);
                ControllerUtils.deleteUserpicFile(oldUserpicFilename, uploadsPath);
            }
        }

        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        return "/profile";
    }
}
