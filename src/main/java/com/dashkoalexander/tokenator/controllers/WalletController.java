package com.dashkoalexander.tokenator.controllers;

import com.dashkoalexander.tokenator.exceptions.ExceptionType;
import com.dashkoalexander.tokenator.exceptions.ExpectedException;
import com.dashkoalexander.tokenator.exceptions.ExpectedExceptionMethodCall;
import com.dashkoalexander.tokenator.models.DTO.ERC20TokenTransactionDTO;
import com.dashkoalexander.tokenator.models.DTO.EthTransactionDTO;
import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.models.Wallet;
import com.dashkoalexander.tokenator.services.SmartContractService;
import com.dashkoalexander.tokenator.services.WalletService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.BigInteger;

/**class {@code WalletController} utilizes the WalletServiceImpl
 * and EthereumInteractionServiceImpl beans for enabling user's
 * wallet-related operations
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.WalletServiceImpl
 */
@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WalletController {

    private static final String ETH_TRANSACTION_DESCRIPTION = "The transaction to %s address from your %s wallet with %s ETH was submitted to the Ethereum network.";
    private static final String ERC20_TOKEN_TRANSACTION_DESCRIPTION = "The transaction to %s address from your %s wallet with %s tokens (token address: %s) was submitted to the Ethereum network.";

    private final WalletService walletService;
    private final SmartContractService smartContractService;

    @Value("${transaction.explorer}")
    private String transactionExplorer;

    @Value("${address.explorer}")
    private String addressExplorer;

    @GetMapping("/wallets")
    public String getUserWallets(
            @AuthenticationPrincipal User user,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ){
        putUserWalletsToModel(model, user, pageable);
        return "/wallets";
    }

    @PostMapping("/create-wallet")
    public String createWallet(
            @AuthenticationPrincipal User user,
            @Valid Wallet wallet,
            BindingResult bindingResult,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        if(bindingResult.hasErrors()) {
            ControllerUtils.putErrorsToModel(bindingResult, model);
            model.addAttribute("wallet", wallet);
        } else {
            model.addAttribute("wallet", null);
            walletService.createWallet(wallet.getName(), user);
        }

        putUserWalletsToModel(model, user, pageable);
        return "/wallets";
    }

    @GetMapping("/wallets/{id}")
    public String getUserWalletAndSmartContracts(
            @AuthenticationPrincipal User user,
            @PathVariable(name = "id", required = true) Wallet wallet,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ){
        checkUserWallet(user, wallet);

        String userWalletAddress = walletService.getWalletAddress(wallet, user);
        putUserWalletsAndSmartContractsToModel(model, user, wallet, userWalletAddress, pageable);
        return "/wallet";
    }

    @PostMapping("/wallets/{id}/send-eth")
    public String sendEth(
            @AuthenticationPrincipal User user,
            @PathVariable(name = "id", required = true) Wallet wallet,
            @RequestParam(name = "userWalletAddress", required = true) String userWalletAddress,
            @Valid EthTransactionDTO ethTransactionDTO,
            BindingResult bindingResult,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        checkUserWallet(user, wallet);

        boolean isBadInput = checkUserInput(() -> { new BigDecimal(ethTransactionDTO.getEthAmount()); },
                                "Amount of ETH should be a positive value.",
                                    "ethAmountError",
                                            bindingResult,
                                            model);

        if(isBadInput) {
            model.addAttribute("ethTransaction", ethTransactionDTO);
            putUserWalletsAndSmartContractsToModel(model, user, wallet, userWalletAddress, pageable);
            return "/wallet";
        } else {
            String transactionHash = walletService.sendEth(wallet, user, ethTransactionDTO.getEthRecipientAddress(), new BigDecimal(ethTransactionDTO.getEthAmount()));
            String transactionDescription = String.format(ETH_TRANSACTION_DESCRIPTION, ethTransactionDTO.getEthRecipientAddress(), userWalletAddress, ethTransactionDTO.getEthAmount());

            if(transactionHash.startsWith("0x")) {
                model.addAttribute("transactionDescription", transactionDescription);
                model.addAttribute("transactionHash", transactionHash);
                model.addAttribute("transactionExplorer", transactionExplorer + transactionHash);
            } else {
                model.addAttribute("errorDescription", transactionHash);
            }

            return "/transaction";
        }
    }

    @PostMapping("/wallets/{id}/send-tokens")
    public String sendERC20Tokens(
            @AuthenticationPrincipal User user,
            @PathVariable(name = "id", required = true) Wallet wallet,
            @RequestParam(name = "userWalletAddress", required = true) String userWalletAddress,
            @RequestParam(name = "smartContractAddress", required = true) String smartContractAddress,
            @Valid ERC20TokenTransactionDTO erc20TokenTransactionDTO,
            BindingResult bindingResult,
            Model model,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        checkUserWallet(user, wallet);

        boolean isBadInput = checkUserInput(() -> { new BigInteger(erc20TokenTransactionDTO.getTokenAmount()); },
                                        "Amount of tokens should be a positive value.",
                                            "tokenAmountError",
                                                    bindingResult,
                                                    model);

        if(isBadInput) {
            model.addAttribute("erc20TokenTransaction", erc20TokenTransactionDTO);
            putUserWalletsAndSmartContractsToModel(model, user, wallet, userWalletAddress, pageable);
            return "/wallet";
        } else {
            String transactionHash = smartContractService.sendERC20Tokens(wallet, user, smartContractAddress, erc20TokenTransactionDTO.getTokenRecipientAddress(), new BigInteger(erc20TokenTransactionDTO.getTokenAmount()));
            String transactionDescription = String.format(ERC20_TOKEN_TRANSACTION_DESCRIPTION, erc20TokenTransactionDTO.getTokenRecipientAddress(), userWalletAddress, erc20TokenTransactionDTO.getTokenAmount(), smartContractAddress);

            if(transactionHash.startsWith("0x")) {
                model.addAttribute("transactionDescription", transactionDescription);
                model.addAttribute("transactionHash", transactionHash);
                model.addAttribute("transactionExplorer", transactionExplorer + transactionHash);
            } else {
                model.addAttribute("errorDescription", transactionHash);
            }

            return "/transaction";
        }
    }

    private void checkUserWallet(User user, Wallet wallet)
    {
        if(wallet == null || !wallet.getUser().equals(user)) {
            throw new ExpectedException(ExceptionType.NOT_FOUND);
        }
    }

    private <T> boolean checkUserInput(ExpectedExceptionMethodCall expectedExceptionMethodCallWithReturn, String errorMessage, String errorField, BindingResult bindingResult, Model model)
    {
        boolean isBadInput = false;
        try {
            expectedExceptionMethodCallWithReturn.action();
        } catch (Exception ex) {
            isBadInput = true;
            model.addAttribute(errorField, errorMessage);
        }

        if(bindingResult.hasErrors()) {
            isBadInput = true;
            ControllerUtils.putErrorsToModel(bindingResult, model);
        }

        return  isBadInput;
    }

    private void putUserWalletsToModel(Model model, User user, Pageable pageable)
    {
        Page<Wallet> wallets = walletService.getUserWallets(user, pageable);
        walletService.getWalletAddresses(wallets, user);

        model.addAttribute("page", wallets);
        model.addAttribute("url", "/wallets");
        model.addAttribute("addressExplorer", addressExplorer);
    }

    private void putUserWalletsAndSmartContractsToModel(Model model, User user, Wallet wallet, String userWalletAddress, Pageable pageable)
    {
        model.addAttribute("wallet", wallet);
        model.addAttribute("userWalletAddress", userWalletAddress);
        model.addAttribute("walletBalance", walletService.getWalletBalance(userWalletAddress).toString());

        Page<SmartContract> smartContracts = smartContractService.getUserSmartContracts(user, pageable);
        smartContractService.getSmartContractBalances(userWalletAddress, smartContracts, user);
        smartContractService.getSmartContractNames(userWalletAddress, smartContracts, user);

        model.addAttribute("page", smartContracts);
        model.addAttribute("url", "/wallet");
        model.addAttribute("addressExplorer", addressExplorer);
    }
}
