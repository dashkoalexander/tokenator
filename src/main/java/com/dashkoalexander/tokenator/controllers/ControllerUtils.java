package com.dashkoalexander.tokenator.controllers;

import com.dashkoalexander.tokenator.exceptions.ExpectedExceptionMethodExecutor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**class {@code ControllerUtils} provides common functionality used
 * among few controllers
 *
 * @author Alexander Dashko
 */
public class ControllerUtils {

    /**
     * used for displaying validation errors back to the user
     */
    public static void putErrorsToModel(BindingResult bindingResult, Model model) {
        Map<String, String> errors = bindingResult.getFieldErrors()
                                                .stream()
                                                .collect(Collectors.toMap(error -> error.getField() + "Error",
                                                                        FieldError::getDefaultMessage,
                                                                        (error1, error2) -> error1));
        errors.entrySet().forEach(w -> model.addAttribute(w.getKey(), w.getValue()));;
    }

    public static String saveUserpicFile(MultipartFile file, String uploadsPath) {
        return ExpectedExceptionMethodExecutor.<String>execute(() -> uploadFile(file, uploadsPath));
    }

    private static String uploadFile(MultipartFile file, String uploadsPath) throws IOException {
        String resultFilename = "";
        if (file != null && !StringUtils.isBlank(file.getOriginalFilename())) {
            File uploadDir = new File(uploadsPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadsPath + resultFilename));
        }

        return resultFilename;
    }

    public static void deleteUserpicFile(String filename, String uploadsPath) {
        if(!StringUtils.isBlank(filename))
        {
            File file = new File(uploadsPath + filename);
            if(file.exists())
            {
                file.delete();
            }
        }
    }
}
