package com.dashkoalexander.tokenator.controllers;

import com.dashkoalexander.tokenator.models.DTO.CaptchaResponseDTO;
import com.dashkoalexander.tokenator.models.User;
import com.dashkoalexander.tokenator.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collections;

/**class {@code RegistrationController} utilizes the UserServiceImpl bean
 * for registering new users
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.UserServiceImpl
 */
@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/registration")
public class RegistrationController {

    private static final String CAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    private final UserService userService;
    private final RestTemplate restTemplate;

    @Value("${recaptcha.secret}")
    private String recaptchaSecret;

    @GetMapping
    public String registration()
    {
        return "/registration";
    }

    @PostMapping
    public String registerNewUser(
            @RequestParam (name = "g-recaptcha-response", required = true) String recaptchaResponse,
            @Valid User user,
            BindingResult bindingResult,
            Model model)
    {
        boolean validatedInput = true;
        String url = String.format(CAPTCHA_URL, recaptchaSecret, recaptchaResponse);
        CaptchaResponseDTO response = restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDTO.class);

        if(!response.isSuccess()) {
            model.addAttribute("captchaError", "Captcha is not solved correctly.");
        }

        if(user.getPassword() != null &&
                user.getPasswordConfirmation() != null &&
                !user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("passwordConfirmationError", "Passwords don't match.");
            validatedInput = false;
        }

        if(bindingResult.hasErrors()) {
            ControllerUtils.putErrorsToModel(bindingResult, model);
            validatedInput = false;
        }

        if(validatedInput) {
            if(!userService.createUser(user)) {
                model.addAttribute("usernameError", "User exists already.");
            } else {
                model.addAttribute("messageType", "success");
                model.addAttribute("message", "You can confirm your e-mail with a link we've just sent to it.");
                return "/login";
            }
        }

        model.addAttribute("hidenavbar", true);
        return "/registration";
    }

    @GetMapping("/activate/{code}")
    public String activate(
            @PathVariable String code,
            Model model
    ) {
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "User successfully activated");
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Activation code is not found!");
        }

        return "/login";
    }
}
