package com.dashkoalexander.tokenator.models.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ERC20TokenTransactionDTO {

    @NotBlank(message = "Recipient Ethereum address is required.")
    @Length(max = 42, min = 42, message = "Ethereum address consists of 42 symbols.")
    private String tokenRecipientAddress;

    @NotBlank(message = "Amount of tokens to send is required.")
    private String tokenAmount;
}
