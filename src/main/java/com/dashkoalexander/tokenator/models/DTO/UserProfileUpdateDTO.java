package com.dashkoalexander.tokenator.models.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileUpdateDTO {

    @Length(max = 20, message = "Password is too long - should be 20 symbols max.")
    private String password;

    @Email(message = "Invalid email.")
    @Length(max = 50, message = "Email is too long - should be 50 symbols max.")
    private String email;
}
