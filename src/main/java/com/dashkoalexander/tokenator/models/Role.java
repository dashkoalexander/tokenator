package com.dashkoalexander.tokenator.models;

import org.springframework.security.core.GrantedAuthority;

/**enum {@code Role} is used inside the User class to determine
 * user's level of access
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.models.User
 */
public enum Role implements GrantedAuthority {

    USER, ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }
}
