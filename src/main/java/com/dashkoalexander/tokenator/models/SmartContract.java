package com.dashkoalexander.tokenator.models;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "smart_contracts")
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "id" })
public class SmartContract extends BaseEntity {

    @Column(name = "address")
    @NotBlank(message = "Token Ethereum address is required.")
    @Length(max = 42, min = 42, message = "Token Ethereum address consists of 42 symbols.")
    private String address;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_smart_contracts",
            joinColumns = { @JoinColumn(name = "smart_contract_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id")}
    )
    private Set<User> users = new HashSet<>();

    @Transient
    private BigInteger balance;

    @Transient
    private String name;
}
