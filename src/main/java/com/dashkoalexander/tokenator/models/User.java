package com.dashkoalexander.tokenator.models;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "id" })
public class User extends BaseEntity implements UserDetails {

    @Column(name = "username")
    @NotBlank(message = "Username is required.")
    @Length(max = 20, message = "Username is too long - should be 20 symbols max.")
    private String username;

    @Column(name = "password")
    @NotBlank(message = "Password is required.")
    private String password;

    @Transient
    @NotBlank(message = "Password confirmation is required.", groups = User.class)
    private String passwordConfirmation;

    @Column(name = "email")
    @Email(message = "Invalid email.")
    @NotBlank(message = "Email is required.")
    @Length(max = 50, message = "Email is too long - should be 50 symbols max.")
    private String email;

    @Column(name = "activation_code")
    private String activationCode;

    @Column(name = "active")
    private boolean active;

    @Column(name = "locked")
    private boolean locked;

    @Column(name = "userpic")
    private String userpicFilename;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    public boolean isAdmin() {
        return roles.contains(Role.ADMIN);
    }

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Wallet> wallets;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_smart_contracts",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "smart_contract_id")}
    )
    private Set<SmartContract> smartContracts = new HashSet<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !isLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive();
    }
}
