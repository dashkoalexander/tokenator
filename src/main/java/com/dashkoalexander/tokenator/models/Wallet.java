package com.dashkoalexander.tokenator.models;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "wallets")
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor()
@EqualsAndHashCode(of = { "id" })
public class Wallet extends BaseEntity {

    @Column(name = "filename")
    private String filename;

    @Column(name = "name")
    @NotBlank(message = "Wallet name is required.")
    @Length(max = 80, message = "Wallet name length shouldn't exceed 80 symbols.")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Transient
    private String address;
}
