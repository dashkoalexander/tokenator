package com.dashkoalexander.tokenator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**class {@code MvcConfig} is a configuration class responsible for
 * managing custom view/controller relations and static resource
 * handling
 *
 * @author Alexander Dashko
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Bean
    public RestTemplate getRestTemplate()
    {
        return new RestTemplate();
    }

    @Value("${uploads.path}")
    private String uploadsPath;

    public void addViewControllers(ViewControllerRegistry viewControllerRegistry) {
        viewControllerRegistry.addViewController("/login").setViewName("login");
        viewControllerRegistry.addRedirectViewController("/", "/wallets");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry resourceHandlerRegistry)
    {
        resourceHandlerRegistry.addResourceHandler("/img/**")
                                .addResourceLocations("file:///" + uploadsPath + "/");
    }
}