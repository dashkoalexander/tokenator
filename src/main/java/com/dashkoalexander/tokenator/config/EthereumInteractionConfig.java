package com.dashkoalexander.tokenator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

/**class {@code EthereumInteractionConfig} provides the Web3J bean for interacting
 * with Ethereum network and it's smart contracts
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.EthereumInteractionServiceImpl
 */
@Configuration
public class EthereumInteractionConfig {

    private static final String INFURA_URL = "https://%s.infura.io/v3/%s";

    @Value("${insfura.apikey}")
    private String infuraApiKey;

    @Value("${insfura.domain}")
    private String infuraDomain;

    @Bean
    public Web3j getWeb3J() {
        String url = String.format(INFURA_URL, infuraDomain, infuraApiKey);
        return Web3j.build(new HttpService(url));
    }
}
