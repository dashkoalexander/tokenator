package com.dashkoalexander.tokenator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**class {@code EncryptionConfig} provides the PasswordEncoder bean for
 * encrypting user passwords in a database
 *
 * @author Alexander Dashko
 * @see com.dashkoalexander.tokenator.services.UserServiceImpl
 */
@Configuration
public class EncryptionConfig {

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder(8);
    }
}