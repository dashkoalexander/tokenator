package com.dashkoalexander.tokenator.rest;

import com.dashkoalexander.tokenator.models.SmartContract;
import com.dashkoalexander.tokenator.services.SmartContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/smart-contracts")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SmartContractRestController {

    private final SmartContractService smartContractService;

    @RequestMapping(value = "{address}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Integer> getSmartContractUsersAmountByAddress(@PathVariable("address") String address)
    {
        SmartContract smartContract = smartContractService.getByAddress(address);
        if(smartContract == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Map<String, Integer> response = new HashMap<>();
        response.put("users", smartContract.getUsers().size());

        return new ResponseEntity(response, HttpStatus.OK);
    }
}
