## Tokenator

Ethereum ERC-20 account manager based on Spring Boot, Web3J, Hibernate/JPA + PostgreSQL, Freemarker + Bootstrap, Lombok, Flyway.

User can manage own ETH/ERC-20 balances for the selected wallet and a set of added token addresses as follows:

![Wallet screen with all main operations](/source/images/main.jpg "Wallet screen with all main operations")

Transaction confirmation examples (with a clickable link to review the transaction state on Etherscan via transaction hash):

![Successful ETH transaction](/source/images/eth.jpg "Successful ETH transaction")

![Successful ERC-20 Token transaction](/source/images/erc20.jpg "Successful ERC-20 Token transaction")

Failing transaction handling (get richER to send transaction or die tryin, lol):

![Failed transaction](/source/images/failed.jpg "Failed transaction")